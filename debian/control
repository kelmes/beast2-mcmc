Source: beast2-mcmc
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Olivier Sallou <osallou@debian.org>,
           Pierre Gruet <pgt@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               javahelper,
               ant,
               ant-optional,
               default-jdk,
               libjam-java,
               libhmsbeagle-java,
               libcolt-free-java,
               libfest-util-java,
               libantlr4-runtime-java,
               junit4,
               texlive-latex-base,
               texlive-latex-extra
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/med-team/beast2-mcmc
Vcs-Git: https://salsa.debian.org/med-team/beast2-mcmc.git
Homepage: https://www.beast2.org
Rules-Requires-Root: no

Package: beast2-mcmc
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libhmsbeagle-java,
         libjam-java,
         libcolt-free-java,
         libantlr4-runtime-java,
         ${java:Depends},
         default-jre | java9-runtime
Suggests: beast2-mcmc-doc
Description: Bayesian MCMC phylogenetic inference
 BEAST is a cross-platform program for Bayesian MCMC analysis of molecular
 sequences. It is entirely orientated towards rooted, time-measured
 phylogenies inferred using strict or relaxed molecular clock models. It
 can be used as a method of reconstructing phylogenies but is also a
 framework for testing evolutionary hypotheses without conditioning on a
 single tree topology. BEAST uses MCMC to average over tree space, so that
 each tree is weighted proportional to its posterior probability. Included
 is a simple to use user-interface program for setting up standard
 analyses and a suit of programs for analysing the results.
 .
 This is no new upstream version of beast-mcmc (1.x) but rather a rewritten
 version.

Package: beast2-mcmc-examples
Architecture: all
Section: doc
Depends: ${shlibs:Depends},
         ${misc:Depends}
Enhances: beast2-mcmc
Multi-Arch: foreign
Description: Bayesian MCMC phylogenetic inference - example data
 BEAST is a cross-platform program for Bayesian MCMC analysis of molecular
 sequences. It is entirely orientated towards rooted, time-measured
 phylogenies inferred using strict or relaxed molecular clock models. It
 can be used as a method of reconstructing phylogenies but is also a
 framework for testing evolutionary hypotheses without conditioning on a
 single tree topology. BEAST uses MCMC to average over tree space, so that
 each tree is weighted proportional to its posterior probability. Included
 is a simple to use user-interface program for setting up standard
 analyses and a suit of programs for analysing the results.
 .
 This package contains the example data.

Package: beast2-mcmc-doc
Architecture: all
Section: doc
Depends: ${shlibs:Depends},
         ${misc:Depends}
Enhances: beast2-mcmc
Multi-Arch: foreign
Description: Bayesian MCMC phylogenetic inference - documentation
 BEAST is a cross-platform program for Bayesian MCMC analysis of molecular
 sequences. It is entirely orientated towards rooted, time-measured
 phylogenies inferred using strict or relaxed molecular clock models. It
 can be used as a method of reconstructing phylogenies but is also a
 framework for testing evolutionary hypotheses without conditioning on a
 single tree topology. BEAST uses MCMC to average over tree space, so that
 each tree is weighted proportional to its posterior probability. Included
 is a simple to use user-interface program for setting up standard
 analyses and a suit of programs for analysing the results.
 .
 This package contains the documentation.
